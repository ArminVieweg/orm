# -*- mode: ruby -*-
# vi: set ft=ruby :

# Requires to perform this first once:
# `vagrant plugin install vagrant-winnfsd`
# `vagrant plugin install vagrant-bindfs`

Vagrant.configure("2") do |config|
    config.vm.box = "ArminVieweg/ubuntu-xenial64-lamp"

    config.vm.network "forwarded_port", guest: 80, host: 8080

    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder ".", "/var/nfs", type: "nfs"

    config.bindfs.bind_folder "/var/nfs", "/vagrant"
    config.bindfs.bind_folder "/var/nfs", "/var/www/html/typo3conf/ext/orm"
    config.bindfs.bind_folder "/var/nfs", "/var/www/html_nocomposer/typo3conf/ext/orm"

    config.bindfs.default_options = {
        force_user:   "vagrant",
        force_group:  "www-data",
        perms:        "u=rwX:g=rwX:o=rD"
    }

    config.vm.network "private_network", type: "dhcp"

    config.vm.provider "virtualbox" do |vb|
        vb.memory = 4096
        vb.cpus = 2
    end

    # Run always
    config.vm.provision "shell", run: "always", inline: <<-SHELL
        cd ~
        sudo composer self-update --no-progress
    SHELL

    # Run once (install DCE in /var/www/html)
    config.vm.provision "shell", inline: <<-SHELL
        cd /var/www/html
        echo "{}" > composer.json

        # Add packages to required section in composer.json
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "typo3/cms" "^8.7"
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "helhum/typo3-console" "^4.5"
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "doctrine/orm" "^2.5"
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["require"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "symfony/process" "^3.2"

        echo "Fetching TYPO3 8.7 using composer and install it with typo3_console..."
        composer update --no-progress -n -q
        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3_orm" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "EXT:orm Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush

        # Add PSR-4 autoloading entries in composer.json
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["autoload"]["psr-4"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "ArminVieweg\\\\Orm\\\\" typo3conf/ext/orm/Classes
        php -r '$f=json_decode(file_get_contents($argv[1]),true);$f["autoload"]["psr-4"][$argv[2]]=$argv[3];file_put_contents($argv[1],json_encode($f,448)."\n");' composer.json "TYPO3\\\\CMS\\\\Orm\\\\DomainObject\\\\" typo3conf/ext/orm/Classes/DomainObject
        composer dump -o

        chmod 2775 . ./typo3conf ./typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        php typo3/cli_dispatch.phpsh extbase extension:install orm
    SHELL

    config.vm.provision "shell", inline: <<-SHELL
        echo "Downloading TYPO3 from get.typo3.org, to /var/www/html_nocomposer..."

        mkdir -p /var/www/html_nocomposer
        cd /var/www/html_nocomposer

        curl -s -L -o typo3_src.tgz get.typo3.org/8.7
        tar --strip-components=1 -xzf typo3_src.tgz
        rm typo3_src.tgz

        chmod 2775 . ./typo3conf ./typo3conf/ext
        chown -R vagrant .
        chgrp -R vagrant .

        echo -e "Alias /no-composer \"/var/www/html_nocomposer/\"\n<Directory \"/var/www/html_nocomposer/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/no-composer-alias.conf
        a2enconf no-composer-alias
        echo "Restarting apache2..."
        service apache2 restart

        touch FIRST_INSTALL
    SHELL

    config.vm.provision "shell", inline: <<-SHELL
        # Add /adminer alias
        composer require vrana/adminer -d /home/vagrant/.composer/ -o --no-progress
        ln -s /home/vagrant/.composer/vendor/vrana/adminer/adminer /var/www/adminer

        echo -e "Alias /adminer \"/var/www/adminer/\"\n<Directory \"/var/www/adminer/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/adminer.conf
        a2enconf adminer
        echo "Restarting apache2..."
        service apache2 restart
    SHELL

end
