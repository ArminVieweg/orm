Doctrine2 ORM Support for TYPO3 CMS
===================================

EXT:orm provides Doctrine2 ORM support for TYPO3 CMS. It hooks to database compare and inject doctrine2's sql,
generated from models in installed extensions, located in Classes/Domain/Model

**Early state of development.**


.. figure:: https://bitbucket.org/repo/p488RGB/images/4104449886-doctrine2-typo3.gif



What is does this extension do?
-------------------------------

Doctrine2 ORM allows you to generate database schema from your php models (annotations) or yaml/xml configuration.

This makes the ext_tables.sql file in TYPO3 extensions obsolete.
Because the annotations are generic, you keep compatibility to other database systems.

When EXT:orm is installed, it hooks itself to TYPO3's **database compare** functionality, located in install tool.
So all tables for your models, you've told to get used by Doctrine2, will be handled by **database compare**.

Also EXT:orm provides a command controller, which allows you to perform doctrine2 actions from commandline without
usage of composer.

The TER releases of EXT:orm will contain vendor binaries.


Installation
------------

The recommended way of installation, is to **use composer**:

.. code-block::

    composer require arminvieweg/orm
    vendor/bin/typo3 extensionmanager:extension:install orm


You can also fetch the extension **from TER** of TYPO3. There all necessary files are already included.
*Symfony process* must work on the system.


CLI usage
---------

To check the current differences between your models/annotations and database, use:

.. code-block::

    vendor/bin/typo3 orm:doctrine2:update


In case you want to actual update the datebase schema, append `--force` to the call.
**CAUTION:** This may break functionality. It's like a database compare from install tool.

When you are using the **TER version**, the call is:

.. code-block::

    typo3/cli_dispatch.phpsh extbase doctrine2:update



Backend integration
-------------------

tbd;


Working with annotations
------------------------

tbw;


Links
-----

* Issue Tracker: https://bitbucket.org/ArminVieweg/orm/issues
* Source code: https://bitbucket.org/ArminVieweg/orm/src
