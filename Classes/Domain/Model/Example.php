<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Domain\Model;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;
use TYPO3\CMS\Orm\DomainObject\Traits;

/**
 * Class Example
 *
 * @package ArminVieweg\Orm
 * @ORM\Entity()
 * @ORM\Table(name="tx_orm_domain_model_example")
 */
class Example extends \TYPO3\CMS\Orm\DomainObject\AbstractEntity
{
    use Traits\EnableColumns;
    use Traits\StarttimeEndtimeColumns;
    use Traits\SortingColumn;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * Get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}
