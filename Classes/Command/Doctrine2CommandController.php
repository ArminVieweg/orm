<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Command;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\Orm\Helper\Doctrine2Helper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class Doctrine2CommandController
 *
 * @package ArminVieweg\Orm\Command
 */
class Doctrine2CommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{
    /**
     * Get list of installed extensions. Imploded by LF
     *
     * @return string
     */
    public function extensionListCommand():string
    {
        return implode(
            LF,
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getLoadedExtensionListArray()
        );
    }

    /**
     * @param bool $force Set to true, to perform updates on database. On false diff is shown.
     * @return string
     */
    public function updateCommand(bool $force = false):string
    {
        $doctrine2Helper = GeneralUtility::makeInstance(Doctrine2Helper::class);
        $updateSql = $doctrine2Helper->getUpdateSql();
        if (!empty($updateSql)) {
            if ($force) {
                return $doctrine2Helper->performUpdates() . PHP_EOL;
            }
            return $updateSql . PHP_EOL . PHP_EOL . 'Append --force to perform updates on database.' . PHP_EOL;
        }
        return 'Database is in sync with current schema.' . PHP_EOL;
    }
}
