<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Controller;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\Orm\Helper\Doctrine2Helper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class BackendController
 *
 * @package ArminVieweg\Orm
 */
class BackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \ArminVieweg\Orm\Helper\GuessPathHelper
     * @inject
     */
    protected $guessPathHelper;

    /**
     * @return string
     */
    public function showAction()
    {
        $doctrine2Helper = GeneralUtility::makeInstance(Doctrine2Helper::class);
        $validateProcess = $doctrine2Helper->validateSchema();

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(
            $validateProcess->getOutput() ?: $validateProcess->getErrorOutput()
        );

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($doctrine2Helper->getUpdateSql(), 'Update SQL');
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($doctrine2Helper->getCreateSql(), 'Full SQL');

        return 'tbd;';
    }
}
