<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Helper;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Singleton LocalConfigurationHelper
 *
 * @package ArminVieweg\Orm
 */
class ConfigurationHelper
{
    const DB_HOST = 'host';
    const DB_USER = 'user';
    const DB_PASSWORD = 'password';
    const DB_CHARSET = 'charset';
    const DB_DATABASE = 'dbname';
    const DB_DRIVER = 'driver';

    /**
     * @var array
     */
    private $databaseCredentials = [];

    /**
     * @param string $option from configuration Default connection. See class DB_ constants
     * @return string Value from database credentials
     * @TODO Support of multiple configurations necessary?
     */
    public function getDatabaseCredential(string $option):string
    {
        $guessPathHelper = new GuessPathHelper();
        $pathLocalConfiguration = $guessPathHelper->guess('typo3conf/LocalConfiguration.php');
        $pathAdditionalConfiguration = $guessPathHelper->guess('typo3conf/AdditionalConfiguration.php');

        if (!empty($pathLocalConfiguration)) {
            $GLOBALS['TYPO3_CONF_VARS'] = require($pathLocalConfiguration);
        }
        if (file_exists($pathAdditionalConfiguration)) {
            require_once($pathAdditionalConfiguration);
        }

        $this->databaseCredentials = $GLOBALS['TYPO3_CONF_VARS']['DB'];

        if (!isset($this->databaseCredentials['Connections']['Default'][$option])) {
            throw new \RuntimeException('No DB option "' . $option . '" found in given configuration files.');
        }
        return $this->databaseCredentials['Connections']['Default'][$option];
    }
}
