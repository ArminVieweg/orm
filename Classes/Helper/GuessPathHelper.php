<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Helper;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Helper class to guess root path of TYPO3 installation
 *
 * @package ArminVieweg\Orm
 */
class GuessPathHelper
{
    /**
     * @var array Paths being checked for requested file
     */
    protected $guessingPaths = [
        // assuming extensions are in typo3conf/ext
        __DIR__ . '/../../../../..',
        // assuming extensions are in e.g. web/typo3conf/ext
        __DIR__ . '/../../../../../..'
    ];

    /**
     * Guesses correct absolute path of given relative one.
     *
     * @param string $path e.g. 'typo3conf/LocalConfiguration.php'
     * @return string Absolute path to file. In last case this method always returns "__PATH__ . '/' . $path".
     */
    public function guess(string $path):string
    {
        $path = ltrim($path, '/\\');
        foreach ($this->guessingPaths as $guessingPath) {
            if (file_exists($guessingPath . '/' . $path)) {
                return realpath($guessingPath . '/' . $path);
            }
        }
        // fallback to orm extension
        return $this->getOrmExtensionPath() . $path;
    }

    /**
     * Returns the path of EXT:orm
     *
     * @return string absolute path to EXT:orm
     */
    public function getOrmExtensionPath():string
    {
        return realpath(__DIR__ . '/../../') . DIRECTORY_SEPARATOR;
    }
}
