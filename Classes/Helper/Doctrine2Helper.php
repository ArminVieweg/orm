<?php
declare(strict_types=1);
namespace ArminVieweg\Orm\Helper;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Process\Process;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper class to execute Doctrine2 commands
 *
 * @package ArminVieweg\Orm
 */
class Doctrine2Helper
{
    /** Exit codes of doctrine orm:validate-schema */
    const STATUS_OK = 0;
    const STATUS_INVALID = 1;
    const STATUS_NOT_IN_SYNC = 2;

    /**
     * @var bool
     */
    protected $autoloaderRequired = false;

    /**
     * Runs vendor binary (composer)
     *
     * @param string $command and arguments/options e.g. 'doctrine help'
     * @return Process Symfony process which ran binary
     */
    protected function runVendorBinary(string $command):Process
    {
        $guessPathHelper = GeneralUtility::makeInstance(GuessPathHelper::class);

        $autoloaderPath = 'typo3conf/ext/orm/vendor/autoload.php';
        if (!$this->autoloaderRequired && file_exists($guessPathHelper->guess($autoloaderPath))) {
            require_once($guessPathHelper->guess($autoloaderPath));
            $this->autoloaderRequired = true;
        }

        $process = new Process(
            // TODO: Do not hardcode /vendor/bin/. Get it from composer.json config
            $guessPathHelper->guess('/vendor/bin/' . $command),
            $guessPathHelper->getOrmExtensionPath()
        );
        $process->run();
        return $process;
    }


    /**
     * Performs doctrine validate-schema
     *
     * @return Process
     */
    public function validateSchema()
    {
        return $this->runVendorBinary('doctrine orm:validate-schema');
    }

    /**
     * Performs validateSchema() and just returns the exit code.
     *
     * @return int|null 0: Validation ok & In sync, 1: Validation failed, 2: Validation ok & Not in sync
     */
    public function getValidateStatus()
    {
        return $this->validateSchema()->getExitCode();
    }

    // TODO: Write getValidateText() method, which also prettifies doctrine2 console output

    /**
     * Analyses models and returns full sql to create all tables and columns.
     * Useful for install tool, which has it's own comparison.
     *
     * @return string Valid SQL or empty string if process did not exit with code 0
     */
    public function getCreateSql():string
    {
        $process = $this->runVendorBinary('doctrine orm:schema-tool:create --dump-sql');
        if (!$process->isSuccessful()) {
            return '';
        }
        return trim($process->getOutput());
    }

    /**
     * Analyses models and returns sql diff (compared with current database).
     *
     * @return string Valid SQL or empty string if process did not exit with code 0
     */
    public function getUpdateSql():string
    {
        $process = $this->runVendorBinary('doctrine orm:schema-tool:update --dump-sql');
        if (!$process->isSuccessful() || strpos(trim($process->getOutput()), 'Nothing to update') === 0) {
            return '';
        }
        return trim($process->getOutput());
    }

    /**
     * Performs updates in database!
     *
     * @return string Process output. In case of error: empty.
     */
    public function performUpdates():string
    {
        $process = $this->runVendorBinary('doctrine orm:schema-tool:update --dump-sql --force');
        if (!$process->isSuccessful() || strpos(trim($process->getOutput()), 'Nothing to update') === 0) {
            return '';
        }
        return trim($process->getOutput());
    }
}
