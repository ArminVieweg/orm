<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractDomainObject
 *
 * @package ArminVieweg\Orm
 * @ORM\MappedSuperclass()
 * @ORM\Table(indexes={@ORM\Index(name="parent", columns={"pid"})})
 */
abstract class AbstractDomainObject extends \TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject
{
    /**
     * @var int The uid of the record. The uid is only unique in the context of the database table.
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;

    /**
     * @var int The id of the page the record is "stored".
     * @ORM\Column(type="integer")
     */
    protected $pid;
}
