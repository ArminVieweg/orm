<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;
use \TYPO3\CMS\Orm\DomainObject\Traits;

/**
 * Class AbstractEntity
 *
 * @package ArminVieweg\Orm
 *
 * @ORM\MappedSuperclass()
 * @ORM\Table(indexes={@ORM\Index(name="t3ver_oid", columns={"t3ver_oid", "t3ver_wsid"})})
 */
abstract class AbstractEntity extends AbstractDomainObject
{
    use Traits\SystemColumns;
    use Traits\VersioningColumns;
    use Traits\TimestampColumns;
}
