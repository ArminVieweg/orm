<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject\Traits;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait which provides system columns "t3_origuid", "cruser_id" and "editlock".
 *
 * @package ArminVieweg\Orm
 */
trait SystemColumns
{
    /**
     * @var int
     * @ORM\Column(name="t3_origuid", type="integer", options={"default": 0})
     */
    protected $t3Origuid = 0;

    /**
     * @var int
     * @ORM\Column(name="cruser_id", type="integer", options={"default": 0})
     */
    protected $cruserId = 0;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $editlock = false;
}
