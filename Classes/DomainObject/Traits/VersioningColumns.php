<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject\Traits;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait which provides columns for versioning.
 *
 * @package ArminVieweg\Orm
 */
trait VersioningColumns
{
    /**
     * @var int
     * @ORM\Column(name="t3ver_oid", type="integer", options={"default": 0})
     */
    protected $t3verOid = 0;

    /**
     * @var int
     * @ORM\Column(name="t3ver_id", type="integer", options={"default": 0})
     */
    protected $t3verId;

    /**
     * @var int
     * @ORM\Column(name="t3ver_wsid", type="integer", options={"default": 0})
     */
    protected $t3verWsid;

    /**
     * @var string
     * @ORM\Column(name="t3ver_label", type="string", length=255, options={"default": ""})
     */
    protected $t3verLabel = '';

    /**
     * @var int
     * @ORM\Column(name="t3ver_state", type="smallint", options={"default": 0})
     */
    protected $t3verState = 0;

    /**
     * @var int
     * @ORM\Column(name="t3ver_stage", type="integer", options={"default": 0})
     */
    protected $t3verStage = 0;

    /**
     * @var int
     * @ORM\Column(name="t3ver_count", type="integer", options={"default": 0})
     */
    protected $t3verCount = 0;

    /**
     * @var int
     * @ORM\Column(name="t3ver_tstamp", type="integer", options={"default": 0})
     */
    protected $t3verTstamp = 0;
    /**
     * @var int
     * @ORM\Column(name="t3ver_move_id", type="integer", options={"default": 0})
     */
    protected $t3verMoveId = 0;
}
