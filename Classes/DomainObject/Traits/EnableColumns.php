<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject\Traits;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait which provides "hidden" and "deleted" columns.
 *
 * @package ArminVieweg\OrmArminVieweg\Orm
 */
trait EnableColumns
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $hidden = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $deleted = false;
}
