<?php
declare(strict_types=1);
namespace TYPO3\CMS\Orm\DomainObject\Traits;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait which provides "crdate" and "tstamp" columns.
 *
 * @package ArminVieweg\Orm
 */
trait TimestampColumns
{
    /**
     * @var int
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $crdate = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $tstamp = 0;
}
