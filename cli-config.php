<?php
declare(strict_types=1);

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2017 Armin Vieweg <armin@v.ieweg.de>
 */

require_once (__DIR__ . '/Classes/Helper/GuessPathHelper.php');
$guessPathHelper = new \ArminVieweg\Orm\Helper\GuessPathHelper();
require_once ($guessPathHelper->guess('vendor/autoload.php'));

use ArminVieweg\Orm\Helper\ConfigurationHelper;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Process\Process;

// TODO: Get Domain/Model folders (from installed extensions)
$paths = [];

$process = new Process('./vendor/bin/typo3 doctrine2:extensionlist', $guessPathHelper->guess(''));
$process->run();
if (!$process->isSuccessful()) {
    $process = new Process('./typo3/cli_dispatch.phpsh extbase doctrine2:extensionList', $guessPathHelper->guess(''));
    $process->run();
}

if ($process->isSuccessful()) {
    foreach (\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode("\n", $process->getOutput(), true) as $extKey) {
        if (file_exists($guessPathHelper->guess('typo3conf/ext/' . $extKey) . '/Classes/Domain/Model')) {
            // 3rd party extension
            // TODO Make paths in extensions configurable
            // TODO Add option in EXT:orm to ignore extensions (to make orm example model, disable-able)
            $paths[] = $guessPathHelper->guess('typo3conf/ext/' . $extKey) . '/Classes/Domain/Model';
        }
    }
}

$configurationHelper = new ConfigurationHelper();

// the connection configuration
$dbParams = array(
    'driver'   => $configurationHelper->getDatabaseCredential(ConfigurationHelper::DB_DRIVER),
    'user'     => $configurationHelper->getDatabaseCredential(ConfigurationHelper::DB_USER),
    'password' => $configurationHelper->getDatabaseCredential(ConfigurationHelper::DB_PASSWORD),
    'dbname'   => $configurationHelper->getDatabaseCredential(ConfigurationHelper::DB_DATABASE),
);

$config = Setup::createAnnotationMetadataConfiguration($paths, false, null, null, false);
// TODO: Add YAML support
//$config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($entityManager->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
));
return $helperSet;
